extends Node

var buildings = []
var clicked_persons = []

var wave_size = 10
var wave_critic_percent = 10
var wave_duration = 3

var crowd_chance = 5
var crowd_size = 5

func _ready():
	#spawn_people(20000, 3, 10)  ne, spaaaß
	spawn_wave()

func spawn_people(count: int, duration: float, critic_percent: int):
	var wait_time = duration/count
	buildings.shuffle()
	while count > 0:
		var idx = (count % buildings.size())
		var person = buildings[idx].spawn_person()
		if randf()*100 <= critic_percent:
			person.critic = true
		$Entities.add_child(person)
		yield(get_tree().create_timer(wait_time),"timeout")
		count -= 1

func spawn_crowd(count: int, duration: float, critic_percent: int):
	var wait_time = duration/count
	var idx = randf() * (buildings.size() - 1)
	while count > 0:
		var person = buildings[idx].spawn_person()
		if randf()*100 <= critic_percent:
			person.critic = true
		$Entities.add_child(person)
		yield(get_tree().create_timer(wait_time),"timeout")
		count -= 1

func spawn_wave():
	$SpawnTimer.wait_time *= 0.9
	spawn_people(wave_size, wave_duration, wave_critic_percent)
	if randf()*100 <= crowd_chance:
		spawn_crowd(crowd_size, wave_duration, wave_critic_percent)
	crowd_chance *= 1.2
	crowd_size *= 1.3
	wave_critic_percent += sqrt(wave_critic_percent)
	wave_size *= 1.2

func _on_SpawnTimer_timeout():
	var entity_counter = -110 # we have round about 110 buildings, but we only want to know the number of people 
	for dude in $Entities.get_children(): # this guard shall protect the game from laging (its not ment to help the player XD)
		entity_counter += 1
	if entity_counter < 2000 :
		spawn_wave()

func person_clicked(person):
	clicked_persons.append(person)
	$ClickTimer.start()

func _on_ClickTimer_timeout():
	var person_to_kill = clicked_persons[0]
	for person in clicked_persons:
		if person.position.y > person_to_kill.position.y:
			person_to_kill = person
	clicked_persons = []
	person_to_kill.queue_free()

