extends Area2D

export var walking_direction: Vector2 = Vector2(0, 0)
var critic: bool = false # set this when instanciating
signal clicked

func _ready():
	connect("clicked", get_parent().get_parent(), "person_clicked")

func _process(delta):
	position += walking_direction * delta

func _on_WalkTimer_timeout():
	_randomize_direction()
	#$WalkTimer.wait_time += 4 * (randf() - 0.5)

func _randomize_direction():
	var direction = Vector2(randf() - 0.5, randf() - 0.5)
	direction = direction.normalized()
	direction *= randf()*10 + 20
	walking_direction = direction

func _on_Person_input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT:
			if event.pressed:
				emit_signal("clicked", self)

func _on_NoCollisionTimer_timeout():
	collision_mask = 2

func _on_building_collided(area):
	if area.collision_layer == 2:
		walking_direction *= -1
