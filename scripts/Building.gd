extends Area2D

export (PackedScene) var person
export var init_vector = Vector2(40, 18) # initial walking wector for persons
export var can_spawn = true

func _ready():
	if scale.x < 0:
		init_vector.x *= -1
	if can_spawn:
		owner.buildings.append(self)

func spawn_person():
	var pers = person.instance()
	pers.position = position
	pers.walking_direction = init_vector
	return pers
