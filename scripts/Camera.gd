extends Camera2D

export var MAX_ZOOM_LEVEL = 0.5
export var MIN_ZOOM_LEVEL = 1.5
export var ZOOM_INCREMENT = 0.05
export var SPEED = 8

var _current_zoom_level: float = 1
var _drag: bool = false

func _process(delta):
	var mouse_pos = get_local_mouse_position()
	var viewport = get_viewport().size/2 * _current_zoom_level
	if mouse_pos.x > viewport.x-2:
		position.x += SPEED
	elif mouse_pos.x < -viewport.x+2:
		position.x -= SPEED
	if mouse_pos.y > viewport.y-2:
		position.y += SPEED
	elif mouse_pos.y < -viewport.y+2:
		position.y -= SPEED
	
	if Input.is_action_pressed("cam_left"):
		position.x -= SPEED
	if Input.is_action_pressed("cam_right"):
		position.x += SPEED
	if Input.is_action_pressed("cam_up"):
		position.y -= SPEED
	if Input.is_action_pressed("cam_down"):
		position.y += SPEED
	
	position.x = min(max(viewport.x, position.x), 3840 - viewport.x)
	position.y = min(max(viewport.y, position.y), 2160 - viewport.y)

func _input(event):
	if event.is_action_pressed("cam_drag"):
		_drag = true
	elif event.is_action_released("cam_drag"):
		_drag = false
	elif event.is_action("cam_zoom_in"):
		_update_zoom(-ZOOM_INCREMENT, Vector2(0,0))
	elif event.is_action("cam_zoom_out"):
		_update_zoom(ZOOM_INCREMENT, Vector2(0,0))
	elif event is InputEventMouseMotion && _drag:
		set_position(get_position() - event.relative*_current_zoom_level)

func _update_zoom(incr, zoom_anchor):
	var old_zoom = _current_zoom_level
	_current_zoom_level += incr
	if _current_zoom_level < MAX_ZOOM_LEVEL:
		_current_zoom_level = MAX_ZOOM_LEVEL
	elif _current_zoom_level > MIN_ZOOM_LEVEL:
		_current_zoom_level = MIN_ZOOM_LEVEL
	if old_zoom == _current_zoom_level:
		return
	
	var zoom_center = zoom_anchor - get_offset()
	var ratio = 1 - _current_zoom_level / old_zoom
	set_offset(get_offset() + zoom_center*ratio)
	
	set_zoom(Vector2(_current_zoom_level, _current_zoom_level))
